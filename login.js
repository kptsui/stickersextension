// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAcvOBptMRETYTk6pv9g37osD_wnMAyCVk',
  authDomain: 'stickerplugin.firebaseapp.com',
  databaseURL: 'https://stickerplugin.firebaseio.com',
  projectId: 'stickerplugin',
  storageBucket: 'stickerplugin.appspot.com',
  messagingSenderId: '584665889599'
};
firebase.initializeApp(config);
const provider = new firebase.auth.GoogleAuthProvider();
const database = firebase.database();
// input elements
const input_login_email = document.getElementById('input_login_email');
const input_login_pw = document.getElementById('input_login_pw');

const input_reg_email = document.getElementById('input_reg_email');
const input_reg_pw = document.getElementById('input_reg_pw');
const input_reg_confirm_pw = document.getElementById('input_reg_confirm_pw');

$(function() {

  $('#login-form-link').click(function(e) {
    e.preventDefault();
    $("#login-form").delay(100).fadeIn(100);
    $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
  });

	$('#register-form-link').click(function(e) {
    e.preventDefault();
    $("#register-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
    $('#login-form-link').removeClass('active');
    $(this).addClass('active');
	});

  $('#login-submit').click(function(e){
    e.preventDefault();
    if(input_login_email.value && input_login_pw.value){
      Signin(input_login_email.value, input_login_pw.value);
    }
  });

  $('#register-submit').click(function(e){
    e.preventDefault();
    if(input_reg_email.value && input_reg_pw.value && input_reg_confirm_pw.value
    && input_reg_pw.value == input_reg_confirm_pw.value){
      SignUp(input_reg_email.value, input_reg_pw.value);
    }
  });

  firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log('User is signed in');
        window.location = "./main.html";
        // /main.html (follow by domain only) vs ./main.html (same prefix)
        // eg. use (/main.html)
        // localhost/Stickers/login.html to=> localhost/main.html
        /*
        user.displayName, user.email, user.emailVerified, user.photoURL;
        user.isAnonymous, user.uid, user.providerData;
        */
      } else {
        console.log('User is signed out');
        // stay to this login page
      }
  }); // End of onAuthStateChanged
});

function Signin(email, password) {
  firebase.auth().signInWithEmailAndPassword(email, password)
  .catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  });
}

function SignUp(email, password) {
  firebase.auth().createUserWithEmailAndPassword(email, password)
  .catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  console.log(errorMessage);
  });
}
