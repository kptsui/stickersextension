// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAcvOBptMRETYTk6pv9g37osD_wnMAyCVk',
  authDomain: 'stickerplugin.firebaseapp.com',
  databaseURL: 'https://stickerplugin.firebaseio.com',
  projectId: 'stickerplugin',
  storageBucket: 'stickerplugin.appspot.com',
  messagingSenderId: '584665889599'
};
firebase.initializeApp(config);
const provider = new firebase.auth.GoogleAuthProvider();
const database = firebase.database();
var currentUser;

const input_email = document.getElementById('input_email');
const input_pw = document.getElementById('input_pw');
const input_imgLink = document.getElementById('imgLink');
const input_tabName = document.getElementById('input_tabName');

$(document).ready(function(){
  initClickEvent();

  selectMode();

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log('User is signed in');
      currentUser = user;
      /*
      user.displayName, user.email, user.emailVerified, user.photoURL;
      user.isAnonymous, user.uid, user.providerData;
      */
      var ref = firebase.database().ref().child(user.uid);
      ref.on('child_added', function(data) { // data.key, data.val()
        appendTab(data.key, data.val().tabName);

        var tabKey = data.key;
        setTabListener(ref.child(data.key).child('stickers'), tabKey);
      });

      ref.on('child_changed', function(data) {
        changeTab(data.key, data.val().tabName);
      });

      ref.on('child_removed', function(data) {
        removeTab(data.key);
      });
    } else {
      console.log('User is signed out');
      if(currentUser)
        firebase.database().ref().child(currentUser.uid).off();

      //$('#nav-tabs').html('<li class="active"><a data-toggle="tab" href="#Common">Common</a></li>');
      //$('#tab-content').html('<div id="Common" class="tab-pane fade in active"></div>');

      window.location = "./login.html";
    }
  }); // End of onAuthStateChanged

});

function appendTab(key, tabName){
  if($('.nav-tabs').children().length > 0){
    $('.nav-tabs').append('<li class="'+key+'"><a data-toggle="tab" href="#'+key+'">'+tabName+'</a></li>');
    $('.tab-content').append('<div class="'+key+' tab-pane fade" id="'+key+'"></div>');
  } else { // active tab
    $('.nav-tabs').append('<li class="'+key+' active"><a data-toggle="tab" href="#'+key+'">'+tabName+'</a></li>');
    $('.tab-content').append('<div class="'+key+' tab-pane fade in active" id="'+key+'"></div>');
  }
  // tab-content for dynamic tab change, use class key for identifying
}
function changeTab(key, tabName){
  $('.nav-tabs > li.'+key+' > a').html(tabName);
}
function removeTab(key){
  $('.'+key).remove();
}

function setTabListener(ref, tabKey){
  ref.on('child_added', function(data) { // data.key, data.val()
    prependSticker(data.key, data.val().url, tabKey);
    selectMode();
  });

  ref.on('child_changed', function(data) {

    changeSticker(data.key, data.val().url);
  });

  ref.on('child_removed', function(data) {

    removeSticker(data.key, data.val().url);
  });
};

function prependSticker(key, url, tabKey){
  $('#'+tabKey).prepend("<span id="+key+" class='sticker_container'>"+
    "<span><img class='sticker' src='"+url+"'></span>"+
    "<span class='overlay'><img src='imgs/cross.png'></span>"+
  "</span>");
}
function changeSticker(key, url){
  $('#'+key).children('span').children('img.sticker').attr('src', url);
}
function removeSticker(key){
  $('#'+key).remove();
}

function deleteMode(){
  var stickers = $('.sticker_container');
  stickers.off("click"); // prevent accumulated listeners

  stickers.click(function(){
    var tabKey = $(this).parent().attr('id');
    database.ref().child(currentUser.uid).child(tabKey).child('stickers').child(this.id).remove();

    console.log('deleteMode: image clicked');
  });

  stickers.addClass('red_border');
  $('.overlay').css('display', 'inline');
  $('#btnSelectMode').css('display', 'inline');
}

function selectMode(){
  var stickers = $('.sticker_container');
  stickers.off("click"); // prevent accumulated listeners

  stickers.click(function(){
    var link = stickers.children('span').children('img.sticker').attr('src');
    copytext(link); // $(this) and this is different!!!

    console.log('selectMode: image clicked, copy ' + link);
  });

  stickers.removeClass('red_border');
  $('.overlay').css('display', 'none');
  $('#btnSelectMode').css('display', 'none');
}

function Signout() {
   firebase.auth().signOut()

   .then(function() {
      console.log('Signout Succesfull')
   }, function(error) {
      console.log('Signout Failed')
   });
}

function add_sticker(){
  var input_url = input_imgLink.value;
  // find the actove tab
  var tabKey = $('.tab-content > div.active').attr('id');
  if(!tabKey){
    alert('Please new a tab before adding sticker!');
    return;
  }
  if(input_url){
    var newStickerRef = database.ref()
    .child(currentUser.uid)
    .child(tabKey)
    .child('stickers')
    .push();
    newStickerRef.set({url: input_url}, function(error){
     if(error){
        console.log(error);
     }
     else{
        console.log("Sticker added successfully");
        input_imgLink.value = '';
      }
    });
  }
}

function add_tab(){
  var inputTabName = input_tabName.value;
  if(inputTabName){
    var newStickerRef = database.ref()
    .child(currentUser.uid)
    .push();
    newStickerRef.set({tabName: inputTabName, stickers:{}}, function(error){
     // Callback comes here
     if(error){
        console.log(error);
     }
     else{
        console.log("Tab added successfully");
        input_tabName.value = '';
      }
    });
  }
}

function show_section_add_sticker(){
  $('#section_add_sticker').css('display', 'block');
}

function close_section_add_sticker(){
  $('#section_add_sticker').css('display', 'none');
}

function show_section_add_tab(){
  $('#section_add_tab').css('display', 'block');
}

function close_section_add_tab(){
  $('#section_add_tab').css('display', 'none');
}

function initClickEvent(){ // Extension does not allow inline click events
  $('#btnSignUp').click(function(){
    if(input_email.value && input_pw.value){
      SignUp(input_email.value, input_pw.value);
    }
  });
  $('#btnLogin').click(function(){
    if(input_email.value && input_pw.value){
      Signin(input_email.value, input_pw.value);
    }
  });
  $('#btnLogout').click(function(){
    Signout();
  });
  $('#btnStartAdd').click(function(){
    show_section_add_sticker();

    selectMode(); // prevent delete action at the same time
  });
  $('#btnRemove').click(function(){
    deleteMode();

    close_section_add_sticker(); // prevent add action at the same time
    close_section_add_tab();
  });
  $('#btnSelectMode').click(function(){
    selectMode();
  });
  $('#btnOk').click(function(){
    close_section_add_sticker();
  });
  $('#btnAdd').click(function(){
    add_sticker();
  });
  $('#btnOkCloseAddTab').click(function(){
    close_section_add_tab();
  });
  $('#btnAddTab').click(function(){
    add_tab();
  });
  $('#btnStartAddTab').click(function(){
    show_section_add_tab();
  });
}

function copytext(text) {
    var textField = document.createElement('textarea');
    textField.innerText = text;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
}
