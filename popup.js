// background must exist first
const background = chrome.extension.getBackgroundPage();
if(background.currentUser){
  switchMainPage();
} else {
  switchLoginPage();
}
$('.nav-tabs').html(background.navTabs.innerHTML);
$('.tab-content').html(background.tabsContainer.innerHTML);
var tabKeys = background.tabKeys;
var stickerKeys = background.stickerKeys;

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAcvOBptMRETYTk6pv9g37osD_wnMAyCVk',
  authDomain: 'stickerplugin.firebaseapp.com',
  databaseURL: 'https://stickerplugin.firebaseio.com',
  projectId: 'stickerplugin',
  storageBucket: 'stickerplugin.appspot.com',
  messagingSenderId: '584665889599'
};
firebase.initializeApp(config);
const provider = new firebase.auth.GoogleAuthProvider();
const database = firebase.database();
var currentUser;

const input_imgLink = document.getElementById('imgLink');
const input_tabName = document.getElementById('input_tabName');

// Login section input elements
const input_login_email = document.getElementById('input_login_email');
const input_login_pw = document.getElementById('input_login_pw');

const input_reg_email = document.getElementById('input_reg_email');
const input_reg_pw = document.getElementById('input_reg_pw');
const input_reg_confirm_pw = document.getElementById('input_reg_confirm_pw');

$(document).ready(function(){
  initClickEvent();
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log('User is signed in');
      currentUser = user;
      if($('.nav-tabs').html() != background.navTabs.innerHTML
      || $('.tab-content').html() != background.tabsContainer.innerHTML){
        $('.nav-tabs').html(background.navTabs.innerHTML);
        $('.tab-content').html(background.tabsContainer.innerHTML);
        console.log('Update view from background.');
      }
      // init sticker listener for each tab that cache in background.js
      // those have not been initialized
      for(var i = 0; i < tabKeys.length; i++){
       setTabListener(firebase.database().ref()
         .child(currentUser.uid)
         .child(tabKeys[i])
         .child('stickers'), tabKeys[i]);
      }
      initListener();
      selectMode();
      //user.displayName, user.email, user.emailVerified, user.photoURL;
      //user.isAnonymous, user.uid, user.providerData;
      switchMainPage();
    } else {
      console.log('User is signed out');
      switchLoginPage();
      $('.nav-tabs').html('');
      $('.tab-content').html('');
    }
  }); // End of onAuthStateChanged

});
/*
var port = chrome.extension.connect({
      name: "Sample Communication"
 });
 port.postMessage("Hi BackGround");
 port.onMessage.addListener(function(data) {
 });
*/
function initListener(){
 var ref = firebase.database().ref().child(currentUser.uid);

 ref.on('child_added', function(data) { // data.key, data.val()
   var tabKey = data.key;

   if(tabKeys.indexOf(tabKey) >= 0){
     return
   }

   appendTab(tabKey, data.val().tabName);

   var stickerRef = firebase.database().ref()
     .child(currentUser.uid)
     .child(tabKey)
     .child('stickers');
   setTabListener(stickerRef, tabKey);
 });

 ref.on('child_changed', function(data) {
   changeTab(data.key, data.val().tabName);
 });

 ref.on('child_removed', function(data) {
   removeTab(data.key);
 });
}

 function setTabListener(ref, tabKey){
   // listen to stickers for each tab
   ref.on('child_added', function(data) { // data.key, data.val()
     var i = tabKeys.indexOf(tabKey);
     if(stickerKeys[i] && stickerKeys[i].indexOf(data.key) >= 0){
       return
     }

     prependSticker(data.key, data.val().url, tabKey);
     selectMode();
   });

   ref.on('child_changed', function(data) {
     changeSticker(data.key, data.val().url);
   });

   ref.on('child_removed', function(data) {
     removeSticker(data.key, data.val().url);
   });
 };

function appendTab(key, tabName){
  if($('.nav-tabs').children().length > 0){
    $('.nav-tabs').append('<li class="'+key+'"><a data-toggle="tab" href="#'+key+'">'+tabName+'</a></li>');
    $('.tab-content').append('<div class="'+key+' tab-pane fade" id="'+key+'"></div>');
  } else { // active tab
    $('.nav-tabs').append('<li class="'+key+' active"><a data-toggle="tab" href="#'+key+'">'+tabName+'</a></li>');
    $('.tab-content').append('<div class="'+key+' tab-pane fade in active" id="'+key+'"></div>');
  }
  // tab-content for dynamic tab change, use class key for identifying
}
function changeTab(key, tabName){
  $('.nav-tabs > li.'+key+' > a').html(tabName);
}
function removeTab(key){
  $('.'+key).remove();
}

function prependSticker(key, url, tabKey){
  $('#'+tabKey).prepend("<span id="+key+" class='sticker_container'>"+
    "<span><img class='sticker' src='"+url+"'></span>"+
    "<span class='overlay'><img src='imgs/cross.png'></span>"+
  "</span>");
}
function changeSticker(key, url){
  $('#'+key).children('span').children('img.sticker').attr('src', url);
}
function removeSticker(key){
  $('#'+key).remove();
}

function deleteMode(){
  var stickers = $('.sticker_container');
  stickers.off("click"); // prevent accumulated listeners

  stickers.click(function(){
    console.log('deleteMode: image clicked');

    var tabKey = $(this).parent().attr('id');
    database.ref().child(currentUser.uid)
    .child(tabKey)
    .child('stickers')
    .child(this.id)
    .remove(function(error){
      if(error){
        console.log(error);
      } else {
        console.log('Removed');
      }
    });
  });

  stickers.addClass('red_border');
  $('.overlay').css('display', 'inline');
  $('#btnSelectMode').css('display', 'inline');
}

function selectMode(){
  var stickers = $('.sticker_container');
  stickers.off("click"); // prevent accumulated listeners

  stickers.click(function(){
    var link = stickers.children('span').children('img.sticker').attr('src');
    copytext(link); // $(this) and this is different!!!

    console.log('selectMode: image clicked, copy ' + link);
  });

  stickers.removeClass('red_border');
  $('.overlay').css('display', 'none');
  $('#btnSelectMode').css('display', 'none');
}

function add_sticker(){
  var input_url = input_imgLink.value;
  // find the actove tab
  var tabKey = $('.tab-content > div.active').attr('id');
  if(!tabKey){
    alert('Please new a tab before adding sticker!');
    return;
  }
  if(input_url){
    var newStickerRef = database.ref()
    .child(currentUser.uid)
    .child(tabKey)
    .child('stickers')
    .push();
    newStickerRef.set({url: input_url}, function(error){
     if(error){
        console.log(error);
     }
     else{
        console.log("Sticker added successfully");
        input_imgLink.value = '';
      }
    });
  }
}

function add_tab(){
  var inputTabName = input_tabName.value;
  if(inputTabName){
    var newStickerRef = database.ref()
    .child(currentUser.uid)
    .push();
    newStickerRef.set({tabName: inputTabName, stickers:{}}, function(error){
     // Callback comes here
     if(error){
        console.log(error);
     }
     else{
        console.log("Tab added successfully");
        input_tabName.value = '';
      }
    });
  }
}

function switchLoginPage(){
  $('#section_user').css('display', 'none');
  $('#section_login').css('display', 'block');
}
function switchMainPage(){
  $('#section_user').css('display', 'block');
  $('#section_login').css('display', 'none');
}

function show_section_add_sticker(){
  $('#section_add_sticker').css('display', 'block');
}

function close_section_add_sticker(){
  $('#section_add_sticker').css('display', 'none');
}

function show_section_add_tab(){
  $('#section_add_tab').css('display', 'block');
}

function close_section_add_tab(){
  $('#section_add_tab').css('display', 'none');
}

function initClickEvent(){ // Extension does not allow inline click events
  $('#btnLogout').click(function(){
    Signout();
  });
  $('#btnStartAdd').click(function(){
    show_section_add_sticker();

    selectMode(); // prevent delete action at the same time
  });
  $('#btnRemove').click(function(){
    deleteMode();

    close_section_add_sticker(); // prevent add action at the same time
    close_section_add_tab();
  });
  $('#btnSelectMode').click(function(){
    selectMode();
  });
  $('#btnOk').click(function(){
    close_section_add_sticker();
  });
  $('#btnAdd').click(function(){
    add_sticker();
  });
  $('#btnOkCloseAddTab').click(function(){
    close_section_add_tab();
  });
  $('#btnAddTab').click(function(){
    add_tab();
  });
  $('#btnStartAddTab').click(function(){
    show_section_add_tab();
  });

  /*
  Login section buttons
  */
  $('#login-form-link').click(function(e) {
    e.preventDefault();
    $("#login-form").delay(100).fadeIn(100);
    $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
  });

	$('#register-form-link').click(function(e) {
    e.preventDefault();
    $("#register-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
    $('#login-form-link').removeClass('active');
    $(this).addClass('active');
	});

  $('#login-submit').click(function(e){
    e.preventDefault();
    if(input_login_email.value && input_login_pw.value){
      Signin(input_login_email.value, input_login_pw.value);
    }
  });

  $('#register-submit').click(function(e){
    e.preventDefault();
    if(input_reg_pw.value != input_reg_confirm_pw.value){
      alert('2 Passwords have to be same!');
    }
    else if(input_reg_email.value && input_reg_pw.value && input_reg_confirm_pw.value){
      SignUp(input_reg_email.value, input_reg_pw.value);
    }
  });
}

function SignUp(email, password) {
  firebase.auth().createUserWithEmailAndPassword(email, password)
  .catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  alert(errorMessage);
  });
}

function Signin(email, password) {
  firebase.auth().signInWithEmailAndPassword(email, password)
  .catch(function(error) {
  if(error){
    var errorCode = error.code;
    var errorMessage = error.message;
    alert(errorMessage);
  } else {

  }
  });
}

function Signout() {
   firebase.auth().signOut()
   .then(function() {
      console.log('Signout Succesfull');
   }, function(error) {
      console.log('Signout Failed');
      alert(errorMessage);
   });
}

function copytext(text) {
    var textField = document.createElement('textarea');
    textField.innerText = text;
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
}
