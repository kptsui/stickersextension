// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAcvOBptMRETYTk6pv9g37osD_wnMAyCVk',
  authDomain: 'stickerplugin.firebaseapp.com',
  databaseURL: 'https://stickerplugin.firebaseio.com',
  projectId: 'stickerplugin',
  storageBucket: 'stickerplugin.appspot.com',
  messagingSenderId: '584665889599'
};
firebase.initializeApp(config);
const provider = new firebase.auth.GoogleAuthProvider();
const database = firebase.database();
var currentUser = "";

var tabKeys = [];
var stickerKeys = [];

var navTabs = document.getElementsByClassName('nav-tabs')[0];
var tabsContainer = document.getElementsByClassName('tab-content')[0];
/*
chrome.extension.onConnect.addListener(function(port) {
      console.log("Connected .....");
      port.onMessage.addListener(function(msg) {
           console.log("message recieved" + msg);

           if(currentUser){
             var cTabKeys = [];
             var cStickerKeys = [];
             for(var i = 0; i < tabKeys.length; i++){
               cTabKeys.push(tabKeys[i]);
               cStickerKeys.push([]);
               for(var j = 0; j < stickerKeys[i].length; j++){
                 cStickerKeys[i].push(stickerKeys[i][j]);
               }
             }

             openPopup();

             port.postMessage({
               cTabKeys: tabKeys,
               cStickerKeys: stickerKeys
             });
             console.log('send tabKeys: ' + tabKeys);
           }
      });
 });

function openPopup(){
  var views = chrome.extension.getViews({
      type: "popup"
  });
  // normally, only 1 view (the popup view)
  for (var i = 0; i < views.length; i++) {
      views[i].document.getElementsByClassName('nav-tabs')[0]
      .innerHTML = navTabs.innerHTML;
      views[i].document.getElementsByClassName('tab-content')[0]
      .innerHTML = tabsContainer.innerHTML;
  }
}
*/

$(document).ready(function(){

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log('User is signed in');
      currentUser = user;
      //user.displayName, user.email, user.emailVerified, user.photoURL;
      //user.isAnonymous, user.uid, user.providerData;
      var ref = firebase.database().ref().child(user.uid);
      ref.on('child_added', function(data) { // data.key, data.val()
        console.log('on child_added tab');
        appendTab(data.key, data.val().tabName);

        var tabKey = data.key;

        // cache key
        tabKeys.push(tabKey);
        stickerKeys.push([]);

        var stickerRef = firebase.database().ref()
          .child(currentUser.uid)
          .child(tabKey)
          .child('stickers');
        setTabListener(stickerRef, tabKey);
      });

      ref.on('child_changed', function(data) {
        changeTab(data.key, data.val().tabName);
      });

      ref.on('child_removed', function(data) {
        removeTab(data.key);

        // remove key in array cache
        var index = tabKeys.indexOf(data.key);
        if (index > -1) {
            tabKeys.splice(index, 1);
            stickerKeys.splice(index, 1);
        }
      });
    } else {
      console.log('User is signed out');
      tabKeys.length = 0;
      stickerKeys.length = 0;

      $('.nav-tabs').html('');
      $('.tab-content').html('');
    }
  }); // End of onAuthStateChanged

});

function setTabListener(ref, tabKey){
  ref.on('child_added', function(data) { // data.key, data.val()
    console.log('on child_added sticker');
    prependSticker(data.key, data.val().url, tabKey);

    var i = tabKeys.indexOf(tabKey);
    stickerKeys[i].push(data.key);
  });

  ref.on('child_changed', function(data) {
    changeSticker(data.key, data.val().url);
  });

  ref.on('child_removed', function(data) {
    removeSticker(data.key, data.val().url);

    var i = tabKeys.indexOf(tabKey);
    var stickerArr = stickerKeys[i];
    var index = stickerArr.indexOf(data.key);
    if (index > -1) {
        stickerArr.splice(index, 1);
    }
  });
};

function appendTab(key, tabName){
  if($('.nav-tabs').children().length > 0){
    $('.nav-tabs').append('<li class="'+key+'"><a data-toggle="tab" href="#'+key+'">'+tabName+'</a></li>');
    $('.tab-content').append('<div class="'+key+' tab-pane fade" id="'+key+'"></div>');
  } else { // active tab
    $('.nav-tabs').append('<li class="'+key+' active"><a data-toggle="tab" href="#'+key+'">'+tabName+'</a></li>');
    $('.tab-content').append('<div class="'+key+' tab-pane fade in active" id="'+key+'"></div>');
  }
  // tab-content for dynamic tab change, use class key for identifying
}
function changeTab(key, tabName){
  $('.nav-tabs > li.'+key+' > a').html(tabName);
}
function removeTab(key){
  $('.'+key).remove();
}

function prependSticker(key, url, tabKey){
  $('#'+tabKey).prepend("<span id="+key+" class='sticker_container'>"+
    "<span><img class='sticker' src='"+url+"'></span>"+
    "<span class='overlay'><img src='imgs/cross.png'></span>"+
  "</span>");
}
function changeSticker(key, url){
  $('#'+key).children('span').children('img.sticker').attr('src', url);
}
function removeSticker(key){
  $('#'+key).remove();
}

function add_sticker(){
  var input_url = input_imgLink.value;
  // find the actove tab
  var tabKey = $('.tab-content > div.active').attr('id');
  if(!tabKey){
    alert('Please new a tab before adding sticker!');
    return;
  }
  if(input_url){
    var newStickerRef = database.ref()
    .child(currentUser.uid)
    .child(tabKey)
    .child('stickers')
    .push();
    newStickerRef.set({url: input_url}, function(error){
     if(error){
        console.log(error);
     }
     else{
        console.log("Sticker added successfully");
        input_imgLink.value = '';
      }
    });
  }
}

function add_tab(){
  var inputTabName = input_tabName.value;
  if(inputTabName){
    var newStickerRef = database.ref()
    .child(currentUser.uid)
    .push();
    newStickerRef.set({tabName: inputTabName, stickers:{}}, function(error){
     // Callback comes here
     if(error){
        console.log(error);
     }
     else{
        console.log("Tab added successfully");
        input_tabName.value = '';
      }
    });
  }
}
